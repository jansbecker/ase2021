
#define MAX_TIME 10000
#define MIN(A,B) (A < B -> A : B)
#define MAX(A,B) (A > B -> A : B)

bool step = true;
int delta = 0; // time increment
int delta_min = MAX_TIME; // minimal time increment
int delta_max = MAX_TIME; // maximal time increment

mtype {SUSPENDED, READY, RUNNING, WAITING};
mtype {ACTIVATE, START, PREEMPT, TERMINATE, TIME_STEP, WAIT, RESUME};
mtype states[14] = SUSPENDED;


inline wait(CMIN, CMAX, COND) {
  atomic {
    clk = 0;
    suspended = false;
    do
    :: clk >= CMIN -> COND; break
    :: clk < CMAX  -> 
         if 
         :: COND && step == 0 -> 
            delta_min = MIN(delta_min, MAX(CMIN - clk, 0)); 
            delta_max = MIN(delta_max, CMAX - clk); 
            step == 1; 
            if
            :: true -> clk = clk + delta 
            :: suspended && clk + delta > 0 -> clk = clk + delta - 1
            :: suspended -> clk = clk + delta + 1
            fi;
            suspended = false
         :: !COND && step == 0 && !suspended -> suspended = true
         fi
    od
  }
}
chan send_event_ret = [0] of {bool};

chan comm_Deadline_Task_DASM = [0] of {mtype,int};     
active proctype observer_Deadline_Task_DASM () {
  mtype msg; int data, clk; bool suspended;
  m0:
  do
  :: atomic {comm_Deadline_Task_DASM?msg,data -> 
     if
     :: msg == ACTIVATE && data == 2 -> send_event_ret!1; goto m1
     :: else -> skip
     fi; send_event_ret!1}
  od;
  m1:
  do
  :: atomic {comm_Deadline_Task_DASM?msg,data -> 
     if
     :: msg == TERMINATE && data == 2 -> clk = 0; send_event_ret!1; goto m0
     :: msg == TIME_STEP -> clk = clk + data;
        if 
        :: clk <= 50 -> skip
        :: else -> goto bad
        fi   
     :: else -> skip
     fi; send_event_ret!1}
  od
  bad:
  atomic {
    printf("Deadline_Task_DASM has been violated.\n");
    assert(false)
  }      
}
chan comm_Deadline_Task_CAN = [0] of {mtype,int};     
active proctype observer_Deadline_Task_CAN () {
  mtype msg; int data, clk; bool suspended;
  m0:
  do
  :: atomic {comm_Deadline_Task_CAN?msg,data -> 
     if
     :: msg == ACTIVATE && data == 3 -> send_event_ret!1; goto m1
     :: else -> skip
     fi; send_event_ret!1}
  od;
  m1:
  do
  :: atomic {comm_Deadline_Task_CAN?msg,data -> 
     if
     :: msg == TERMINATE && data == 3 -> clk = 0; send_event_ret!1; goto m0
     :: msg == TIME_STEP -> clk = clk + data;
        if 
        :: clk <= 100 -> skip
        :: else -> goto bad
        fi   
     :: else -> skip
     fi; send_event_ret!1}
  od
  bad:
  atomic {
    printf("Deadline_Task_CAN has been violated.\n");
    assert(false)
  }      
}
chan comm_Deadline_Task_EKF = [0] of {mtype,int};     
active proctype observer_Deadline_Task_EKF () {
  mtype msg; int data, clk; bool suspended;
  m0:
  do
  :: atomic {comm_Deadline_Task_EKF?msg,data -> 
     if
     :: msg == ACTIVATE && data == 4 -> send_event_ret!1; goto m1
     :: else -> skip
     fi; send_event_ret!1}
  od;
  m1:
  do
  :: atomic {comm_Deadline_Task_EKF?msg,data -> 
     if
     :: msg == TERMINATE && data == 4 -> clk = 0; send_event_ret!1; goto m0
     :: msg == TIME_STEP -> clk = clk + data;
        if 
        :: clk <= 150 -> skip
        :: else -> goto bad
        fi   
     :: else -> skip
     fi; send_event_ret!1}
  od
  bad:
  atomic {
    printf("Deadline_Task_EKF has been violated.\n");
    assert(false)
  }      
}
chan comm_Deadline_Task_LIDAR = [0] of {mtype,int};     
active proctype observer_Deadline_Task_LIDAR () {
  mtype msg; int data, clk; bool suspended;
  m0:
  do
  :: atomic {comm_Deadline_Task_LIDAR?msg,data -> 
     if
     :: msg == ACTIVATE && data == 1 -> send_event_ret!1; goto m1
     :: else -> skip
     fi; send_event_ret!1}
  od;
  m1:
  do
  :: atomic {comm_Deadline_Task_LIDAR?msg,data -> 
     if
     :: msg == TERMINATE && data == 1 -> clk = 0; send_event_ret!1; goto m0
     :: msg == TIME_STEP -> clk = clk + data;
        if 
        :: clk <= 330 -> skip
        :: else -> goto bad
        fi   
     :: else -> skip
     fi; send_event_ret!1}
  od
  bad:
  atomic {
    printf("Deadline_Task_LIDAR has been violated.\n");
    assert(false)
  }      
}
chan comm_Deadline_Task_SFM = [0] of {mtype,int};     
active proctype observer_Deadline_Task_SFM () {
  mtype msg; int data, clk; bool suspended;
  m0:
  do
  :: atomic {comm_Deadline_Task_SFM?msg,data -> 
     if
     :: msg == ACTIVATE && data == 6 -> send_event_ret!1; goto m1
     :: else -> skip
     fi; send_event_ret!1}
  od;
  m1:
  do
  :: atomic {comm_Deadline_Task_SFM?msg,data -> 
     if
     :: msg == TERMINATE && data == 6 -> clk = 0; send_event_ret!1; goto m0
     :: msg == TIME_STEP -> clk = clk + data;
        if 
        :: clk <= 330 -> skip
        :: else -> goto bad
        fi   
     :: else -> skip
     fi; send_event_ret!1}
  od
  bad:
  atomic {
    printf("Deadline_Task_SFM has been violated.\n");
    assert(false)
  }      
}
chan comm_Deadline_Task_Detection = [0] of {mtype,int};     
active proctype observer_Deadline_Task_Detection () {
  mtype msg; int data, clk; bool suspended;
  m0:
  do
  :: atomic {comm_Deadline_Task_Detection?msg,data -> 
     if
     :: msg == ACTIVATE && data == 9 -> send_event_ret!1; goto m1
     :: else -> skip
     fi; send_event_ret!1}
  od;
  m1:
  do
  :: atomic {comm_Deadline_Task_Detection?msg,data -> 
     if
     :: msg == TERMINATE && data == 9 -> clk = 0; send_event_ret!1; goto m0
     :: msg == TIME_STEP -> clk = clk + data;
        if 
        :: clk <= 2000 -> skip
        :: else -> goto bad
        fi   
     :: else -> skip
     fi; send_event_ret!1}
  od
  bad:
  atomic {
    printf("Deadline_Task_Detection has been violated.\n");
    assert(false)
  }      
}
chan comm_Deadline_Task_Lane_Detection = [0] of {mtype,int};     
active proctype observer_Deadline_Task_Lane_Detection () {
  mtype msg; int data, clk; bool suspended;
  m0:
  do
  :: atomic {comm_Deadline_Task_Lane_Detection?msg,data -> 
     if
     :: msg == ACTIVATE && data == 8 -> send_event_ret!1; goto m1
     :: else -> skip
     fi; send_event_ret!1}
  od;
  m1:
  do
  :: atomic {comm_Deadline_Task_Lane_Detection?msg,data -> 
     if
     :: msg == TERMINATE && data == 8 -> clk = 0; send_event_ret!1; goto m0
     :: msg == TIME_STEP -> clk = clk + data;
        if 
        :: clk <= 660 -> skip
        :: else -> goto bad
        fi   
     :: else -> skip
     fi; send_event_ret!1}
  od
  bad:
  atomic {
    printf("Deadline_Task_Lane_Detection has been violated.\n");
    assert(false)
  }      
}
chan comm_Deadline_Task_Localization = [0] of {mtype,int};     
active proctype observer_Deadline_Task_Localization () {
  mtype msg; int data, clk; bool suspended;
  m0:
  do
  :: atomic {comm_Deadline_Task_Localization?msg,data -> 
     if
     :: msg == ACTIVATE && data == 7 -> send_event_ret!1; goto m1
     :: else -> skip
     fi; send_event_ret!1}
  od;
  m1:
  do
  :: atomic {comm_Deadline_Task_Localization?msg,data -> 
     if
     :: msg == TERMINATE && data == 7 -> clk = 0; send_event_ret!1; goto m0
     :: msg == TIME_STEP -> clk = clk + data;
        if 
        :: clk <= 4000 -> skip
        :: else -> goto bad
        fi   
     :: else -> skip
     fi; send_event_ret!1}
  od
  bad:
  atomic {
    printf("Deadline_Task_Localization has been violated.\n");
    assert(false)
  }      
}

unsigned trigg_OS_Overhead : 4 = 0
unsigned trigg_Lidar_Grabber : 4 = 0
unsigned trigg_DASM : 4 = 0
unsigned trigg_CANbus_polling : 4 = 0
unsigned trigg_EKF : 4 = 0
unsigned trigg_Planner : 4 = 0
unsigned trigg_PRE_SFM_gpu_POST : 4 = 0
unsigned trigg_PRE_Localization_gpu_POST : 4 = 0
unsigned trigg_PRE_Lane_detection_gpu_POST : 4 = 0
unsigned trigg_PRE_Detection_gpu_POST : 4 = 0
unsigned trigg_SFM : 4 = 0
unsigned trigg_Localization : 4 = 0
unsigned trigg_Lane_detection : 4 = 0
unsigned trigg_Detection : 4 = 0
inline send_event(evt,data) {
  atomic {
    printf("%e (%d)\n", evt, data)
    ; comm_Deadline_Task_DASM!evt,data;send_event_ret?_     
    ; comm_Deadline_Task_CAN!evt,data;send_event_ret?_     
    ; comm_Deadline_Task_EKF!evt,data;send_event_ret?_     
    ; comm_Deadline_Task_LIDAR!evt,data;send_event_ret?_     
    ; comm_Deadline_Task_SFM!evt,data;send_event_ret?_     
    ; comm_Deadline_Task_Detection!evt,data;send_event_ret?_     
    ; comm_Deadline_Task_Lane_Detection!evt,data;send_event_ret?_     
    ; comm_Deadline_Task_Localization!evt,data;send_event_ret?_     
  }
}
/*
This FPS scheduler implementation maintains tasks in a linked list.
nxt[id] points to the successor of task id. Note that the values 
in the list use base 1 and 0 means end of list (so nxt[id] - 1 is the real successor id). 
Because queues from different schedulers are distinct, nxt can be shared among all schedulers. 

This FPS implementation maintains one queue for all tasks.
The variable curr holds the head of queue for this scheduler instance. 
New activated or resumed tasks are inserted behind the last scheduler 
with same or higher priority (lowest priority is 0, higher values indicate higher priority). 
*/
local byte nxt[14] = 0;
local byte prio[14] = 0;
hidden byte prec, succ;
proctype scheduler_FPS(chan comm) {
    byte id,pr,prev;
    byte curr = 0;
    mtype msg;
    do
    :: atomic {
        comm?msg,id,pr ->
        if 
        :: msg==ACTIVATE || msg==RESUME -> 
	       send_event(msg,id); d_step {
	          printf("priority: %d -> %d\n",id,pr);
	          prio[id]=pr;
              /* store currently running task */
	          prev = curr; 
              /* find pair (prec,succ) where the new task 
                 shall be inserted between */
	          prec = 0; succ = curr;
	          do
	          :: succ > 0 && prio[succ-1] >= pr -> 
	              prec = succ; 
	              succ = nxt[succ-1]
	          :: else -> break
	          od;
	          if
	          :: prec == 0 -> // new task is head of queue
	              if
	              :: curr > 0 -> states[curr-1] = READY;
	              :: else -> skip
	              fi; 
	              curr = id+1; 
	              states[id] = RUNNING
	          :: else -> states[id]=READY; nxt[prec-1] = id+1
	          fi;
	          nxt[id] = succ
	       }
           /* inform observers about state changes */
	       if 
	       :: curr != prev && prev > 0 -> send_event(PREEMPT,prev-1);send_event(START,curr-1)
	       :: curr != prev && prev == 0 -> send_event(START,curr-1)
	       :: else -> skip
	       fi
	       
	    :: msg==WAIT || msg==TERMINATE -> send_event(msg,id); d_step {
           assert(curr == id + 1); // only the current task can terminate or wait
           prio[id] = 0; 
           states[id] = (msg==WAIT -> WAITING : SUSPENDED);
           /* remove the task from the head of the queue */
           curr = nxt[id];
           nxt[id] = 0};
           /* put the new head of queue to running, if any */
           if
           :: curr > 0 -> states[curr - 1] = RUNNING;
              send_event(START,curr-1)
           :: else -> skip
           fi
        fi
       }
    od
}

byte event_Lane_detect_GPU = 0;
byte event_Detect = 0;
byte event_SFM = 0;
byte event_Localization_GPU = 0;


inline trigger_SFM_stim() {
        d_step {
           trigg_SFM++
        }
}
inline trigger_Localization_stim() {
        d_step {
           trigg_Localization++
        }
}
inline trigger_detection_stim() {
        d_step {
           trigg_Detection++
        }
}
inline trigger_Lane_detection_stim() {
        d_step {
           trigg_Lane_detection++
        }
}
active proctype stim_periodic_5ms() {
    int clk, t_next; bool suspended;
    atomic {
		int t_next_min = 0;
		t_next = 0;
		do
		:: wait(t_next, t_next, true) ;
		   d_step {
		     t_next = t_next - clk + 50;
		     t_next_min = 0;
		         d_step {
		            trigg_DASM++
		         }
           }
        od
    }
}	
active proctype stim_periodic_10ms() {
    int clk, t_next; bool suspended;
    atomic {
		int t_next_min = 0;
		t_next = 0;
		do
		:: wait(t_next, t_next, true) ;
		   d_step {
		     t_next = t_next - clk + 100;
		     t_next_min = 0;
		         d_step {
		            trigg_CANbus_polling++
		         }
           }
        od
    }
}	
active proctype stim_periodic_15ms() {
    int clk, t_next; bool suspended;
    atomic {
		int t_next_min = 0;
		t_next = 0;
		do
		:: wait(t_next, t_next, true) ;
		   d_step {
		     t_next = t_next - clk + 150;
		     t_next_min = 0;
		         d_step {
		            trigg_Planner++
		     ;       trigg_EKF++
		         }
           }
        od
    }
}	
active proctype stim_periodic_33ms() {
    int clk, t_next; bool suspended;
    atomic {
		int t_next_min = 0;
		t_next = 0;
		do
		:: wait(t_next, t_next, true) ;
		   d_step {
		     t_next = t_next - clk + 330;
		     t_next_min = 0;
		         d_step {
		            trigg_Lidar_Grabber++
		     ;       trigg_PRE_SFM_gpu_POST++
		         }
           }
        od
    }
}	
active proctype stim_periodic_66ms() {
    int clk, t_next; bool suspended;
    atomic {
		int t_next_min = 0;
		t_next = 0;
		do
		:: wait(t_next, t_next, true) ;
		   d_step {
		     t_next = t_next - clk + 660;
		     t_next_min = 0;
		         d_step {
		            trigg_PRE_Lane_detection_gpu_POST++
		         }
           }
        od
    }
}	
active proctype stim_periodic_100ms() {
    int clk, t_next; bool suspended;
    atomic {
		int t_next_min = 0;
		t_next = 0;
		do
		:: wait(t_next, t_next, true) ;
		   d_step {
		     t_next = t_next - clk + 1000;
		     t_next_min = 0;
		         d_step {
		            trigg_OS_Overhead++
		         }
           }
        od
    }
}	
active proctype stim_periodic_200ms() {
    int clk, t_next; bool suspended;
    atomic {
		int t_next_min = 0;
		t_next = 0;
		do
		:: wait(t_next, t_next, true) ;
		   d_step {
		     t_next = t_next - clk + 2000;
		     t_next_min = 0;
		         d_step {
		            trigg_PRE_Detection_gpu_POST++
		         }
           }
        od
    }
}	
active proctype stim_periodic_400ms() {
    int clk, t_next; bool suspended;
    atomic {
		int t_next_min = 0;
		t_next = 0;
		do
		:: wait(t_next, t_next, true) ;
		   d_step {
		     t_next = t_next - clk + 4000;
		     t_next_min = 0;
		         d_step {
		            trigg_PRE_Localization_gpu_POST++
		         }
           }
        od
    }
}	

chan comm_Core3 = [0] of {mtype, byte, byte};
chan comm_Core1 = [0] of {mtype, byte, byte};
chan comm_Core4 = [0] of {mtype, byte, byte};
chan comm_GP10B = [0] of {mtype, byte, byte};
chan comm_Core0 = [0] of {mtype, byte, byte};
chan comm_Core2 = [0] of {mtype, byte, byte};
chan comm_Core5 = [0] of {mtype, byte, byte};

active proctype task_OS_Overhead () {
  int clk; bool suspended;
  do
  :: atomic {trigg_OS_Overhead > 0 -> 
         trigg_OS_Overhead-- ;
         comm_Core5!ACTIVATE,0,0 ;
         states[0] == RUNNING 
     } 
     ; wait(500, 500, states[0] == RUNNING) /* 500.0, 500.0 */
     
     
     
     ; atomic {states[0] == RUNNING ; comm_Core5!TERMINATE,0,0}
  od
}
active proctype task_Lidar_Grabber () {
  int clk; bool suspended;
  do
  :: atomic {trigg_Lidar_Grabber > 0 -> 
         trigg_Lidar_Grabber-- ;
         comm_Core4!ACTIVATE,1,8 ;
         states[1] == RUNNING 
     } 
     ; wait(101, 137, states[1] == RUNNING) /* 101.6, 136.6 */
     
     
     
     ; atomic {states[1] == RUNNING ; comm_Core4!TERMINATE,1,8}
  od
}
active proctype task_DASM () {
  int clk; bool suspended;
  do
  :: atomic {trigg_DASM > 0 -> 
         trigg_DASM-- ;
         comm_Core5!ACTIVATE,2,3 ;
         states[2] == RUNNING 
     } 
     ; wait(12, 19, states[2] == RUNNING) /* 12.99995, 18.59995 */
     
     
     
     ; atomic {states[2] == RUNNING ; comm_Core5!TERMINATE,2,3}
  od
}
active proctype task_CANbus_polling () {
  int clk; bool suspended;
  do
  :: atomic {trigg_CANbus_polling > 0 -> 
         trigg_CANbus_polling-- ;
         comm_Core2!ACTIVATE,3,5 ;
         states[3] == RUNNING 
     } 
     ; wait(3, 6, states[3] == RUNNING) /* 3.9968, 5.9968 */
     
     
     
     ; atomic {states[3] == RUNNING ; comm_Core2!TERMINATE,3,5}
  od
}
active proctype task_EKF () {
  int clk; bool suspended;
  do
  :: atomic {trigg_EKF > 0 -> 
         trigg_EKF-- ;
         comm_Core2!ACTIVATE,4,1 ;
         states[4] == RUNNING 
     } 
     ; wait(39, 48, states[4] == RUNNING) /* 39.7967, 47.5967 */
     
     
     
     ; atomic {states[4] == RUNNING ; comm_Core2!TERMINATE,4,1}
  od
}
active proctype task_Planner () {
  int clk; bool suspended;
  do
  :: atomic {trigg_Planner > 0 -> 
         trigg_Planner-- ;
         comm_Core0!ACTIVATE,5,9 ;
         states[5] == RUNNING 
     } 
     ; wait(95, 125, states[5] == RUNNING) /* 95.367645, 124.367645 */
     
     
     
     ; atomic {states[5] == RUNNING ; comm_Core0!TERMINATE,5,9}
  od
}
active proctype task_PRE_SFM_gpu_POST () {
  int clk; bool suspended;
  do
  :: atomic {trigg_PRE_SFM_gpu_POST > 0 -> 
         trigg_PRE_SFM_gpu_POST-- ;
         comm_Core1!ACTIVATE,6,6 ;
         states[6] == RUNNING 
     } 
     ; wait(25, 32, states[6] == RUNNING) /* 25.75712, 31.77571 */
     
     ; atomic {states[6] == RUNNING; trigger_SFM_stim()}
     ; if
       :: states[6] == RUNNING && (event_SFM != 0) -> skip
       :: atomic {states[6] == RUNNING && !(event_SFM != 0) -> 
            comm_Core1!WAIT,6,6;
            states[6] == WAITING && (event_SFM != 0);
            comm_Core1!RESUME,6,6}
       fi
     ; atomic {states[6] == RUNNING; 
             event_SFM = 0}
     ; wait(28, 36, states[6] == RUNNING) /* 28.34784, 35.32258 */
     
     
     
     ; atomic {states[6] == RUNNING ; comm_Core1!TERMINATE,6,6}
  od
}
active proctype task_PRE_Localization_gpu_POST () {
  int clk; bool suspended;
  do
  :: atomic {trigg_PRE_Localization_gpu_POST > 0 -> 
         trigg_PRE_Localization_gpu_POST-- ;
         comm_Core3!ACTIVATE,7,4 ;
         states[7] == RUNNING 
     } 
     ; wait(31, 90, states[7] == RUNNING) /* 31.3936, 89.3936 */
     
     ; atomic {states[7] == RUNNING; trigger_Localization_stim()}
     ; if
       :: states[7] == RUNNING && (event_Localization_GPU != 0) -> skip
       :: atomic {states[7] == RUNNING && !(event_Localization_GPU != 0) -> 
            comm_Core3!WAIT,7,4;
            states[7] == WAITING && (event_Localization_GPU != 0);
            comm_Core3!RESUME,7,4}
       fi
     ; atomic {states[7] == RUNNING; 
             event_Localization_GPU = 0}
     ; wait(41, 87, states[7] == RUNNING) /* 41.999925, 86.999925 */
     
     
     
     ; atomic {states[7] == RUNNING ; comm_Core3!TERMINATE,7,4}
  od
}
active proctype task_PRE_Lane_detection_gpu_POST () {
  int clk; bool suspended;
  do
  :: atomic {trigg_PRE_Lane_detection_gpu_POST > 0 -> 
         trigg_PRE_Lane_detection_gpu_POST-- ;
         comm_Core1!ACTIVATE,8,2 ;
         states[8] == RUNNING 
     } 
     ; wait(30, 36, states[8] == RUNNING) /* 30.75736, 35.256835 */
     
     ; atomic {states[8] == RUNNING; trigger_Lane_detection_stim()}
     ; if
       :: states[8] == RUNNING && (event_Lane_detect_GPU != 0) -> skip
       :: atomic {states[8] == RUNNING && !(event_Lane_detect_GPU != 0) -> 
            comm_Core1!WAIT,8,2;
            states[8] == WAITING && (event_Lane_detect_GPU != 0);
            comm_Core1!RESUME,8,2}
       fi
     ; atomic {states[8] == RUNNING; 
             event_Lane_detect_GPU = 0}
     ; wait(29, 41, states[8] == RUNNING) /* 29.99816, 40.99748 */
     
     
     
     ; atomic {states[8] == RUNNING ; comm_Core1!TERMINATE,8,2}
  od
}
active proctype task_PRE_Detection_gpu_POST () {
  int clk; bool suspended;
  do
  :: atomic {trigg_PRE_Detection_gpu_POST > 0 -> 
         trigg_PRE_Detection_gpu_POST-- ;
         comm_Core4!ACTIVATE,9,4 ;
         states[9] == RUNNING 
     } 
     ; wait(31, 37, states[9] == RUNNING) /* 31.8928, 36.8956 */
     
     ; atomic {states[9] == RUNNING; trigger_detection_stim()}
     ; states[9] == RUNNING && (event_Detect != 0)
     ; wait(0, 1, states[9] == RUNNING) /* 0.025, 0.025 */
     
     ; atomic {states[9] == RUNNING; 
             event_Detect = 0}
     ; wait(8, 11, states[9] == RUNNING) /* 8.2, 10.2 */
     
     
     
     ; atomic {states[9] == RUNNING ; comm_Core4!TERMINATE,9,4}
  od
}
active proctype task_SFM () {
  int clk; bool suspended;
  do
  :: atomic {trigg_SFM > 0 -> 
         trigg_SFM-- ;
         comm_GP10B!ACTIVATE,10,1 ;
         states[10] == RUNNING 
     } 
     
     ; wait(70, 79, states[10] == RUNNING) /* 70.5, 79.0 */
     
     
     ; atomic {states[10] == RUNNING; 
             event_SFM = 1}
     
     
     ; atomic {states[10] == RUNNING ; comm_GP10B!TERMINATE,10,1}
  od
}
active proctype task_Localization () {
  int clk; bool suspended;
  do
  :: atomic {trigg_Localization > 0 -> 
         trigg_Localization-- ;
         comm_Core3!ACTIVATE,11,4 ;
         states[11] == RUNNING 
     } 
     
     ; wait(3665, 3875, states[11] == RUNNING) /* 3665.196775, 3874.196775 */
     
     
     ; atomic {states[11] == RUNNING; 
             event_Localization_GPU = 1}
     
     
     ; atomic {states[11] == RUNNING ; comm_Core3!TERMINATE,11,4}
  od
}
active proctype task_Lane_detection () {
  int clk; bool suspended;
  do
  :: atomic {trigg_Lane_detection > 0 -> 
         trigg_Lane_detection-- ;
         comm_Core1!ACTIVATE,12,2 ;
         states[12] == RUNNING 
     } 
     
     ; wait(384, 423, states[12] == RUNNING) /* 384.37824, 422.37824 */
     
     
     ; atomic {states[12] == RUNNING; 
             event_Lane_detect_GPU = 1}
     
     
     ; atomic {states[12] == RUNNING ; comm_Core1!TERMINATE,12,2}
  od
}
active proctype task_Detection () {
  int clk; bool suspended;
  do
  :: atomic {trigg_Detection > 0 -> 
         trigg_Detection-- ;
         comm_GP10B!ACTIVATE,13,0 ;
         states[13] == RUNNING 
     } 
     
     ; wait(1080, 1160, states[13] == RUNNING) /* 1080.0, 1160.0 */
     
     
     ; atomic {states[13] == RUNNING; 
             event_Detect = 1}
     
     
     ; atomic {states[13] == RUNNING ; comm_GP10B!TERMINATE,13,0}
  od
}



init {
  printf("STEPS_PER_SECOND (10000)\n");
  atomic {
    run scheduler_FPS(comm_Core3);
    run scheduler_FPS(comm_Core1);
    run scheduler_FPS(comm_Core4);
    run scheduler_FPS(comm_GP10B);
    run scheduler_FPS(comm_Core0);
    run scheduler_FPS(comm_Core2);
    run scheduler_FPS(comm_Core5);
  }
  do
  :: timeout -> d_step {
		  delta_min = MAX_TIME;
		  delta_max = MAX_TIME;
          step = false 
	  }; 
      timeout; 
      atomic {
	      if
		  :: true -> delta = delta_min
		  :: delta_min < delta_max -> delta = delta_max
		  :: delta_min < delta_max -> delta = delta_min + 1
		  fi ;
	      send_event(TIME_STEP,delta)
	      step = true;
	  }
  od
}
