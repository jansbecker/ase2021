# Ase2021

AMALTHEA and PROMELA files for the experiments in 

J. S. Becker: Model Checking AMALTHEA with SPIN. Submitted to ASE 2021

The AMALTHEA models are slightly modified versions of the original ones
shipped with the APP4MC Platform, version 0.9.8, that can be found under

https://git.eclipse.org/c/app4mc/org.eclipse.app4mc.git/tree/

and have been published by the Eclipse APP4MC project under the EPL v2 License
http://www.eclipse.org/legal/epl-2.0.


In the PROMELA files ending in `*_merged.prm`, consecutive `wait()` statements have been merged. 

The results from the publication have been achieved running SPIN version 6.5.1 in a 64Bit 
Linux environment with the command parameters 
`-run -m100000 -Q2`

The deadline violation of the Planner task in the fmtv2019 model is found using a random execution with default parameters. 

For the PROMELA files, the following applies:

(C) 2020 OFFIS Institute for Information Technology

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

